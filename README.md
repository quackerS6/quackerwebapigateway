# QuackerWebAPIGateway

Aggregates results of services to supply info for entire pages to the frontend. Also normalizes the routes so the frontend can access the services through this gateway.
- Part of the quacker project