using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using OcelotApiGateway.Utils;
using Serilog;
using Serilog.Events;
using System;
using System.IO;
using System.Net;



namespace OcelotApiGateway {
    public class Program {
        public static void Main(string[] args) {
            int defaultPort = 5000;
            int httpsPort = defaultPort;

            new WebHostBuilder()
                .ConfigureAppConfiguration((hostingContext, config) => {
                    config
                        .SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
                        .AddJsonFile("appsettings.json", true, true)
                        .AddJsonFile($"secrets/appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", true, true)
                        //.AddOcelot("OcelotConfig", hostingContext.HostingEnvironment)
                        .CreateJsonFile(hostingContext, "OcelotJson", "ocelot.json")
                        .AddJsonFile("ocelot.json", false)
                        .AddEnvironmentVariables();
                })
                .UseKestrel(options => {
                    var configuration = (IConfiguration)options.ApplicationServices.GetService(typeof(IConfiguration));
                    httpsPort = configuration.GetValue("ASPNETCORE_HTTPS_PORT", defaultPort);
                    string path = Environment.GetEnvironmentVariable("ASPNETCORE_Kestrel__Certificates__Default__Path") ?? configuration.GetSection("CertificateDefaults")["Path"];
                    string pass = Environment.GetEnvironmentVariable("ASPNETCORE_Kestrel__Certificates__Default__Password") ?? configuration.GetSection("CertificateDefaults")["Password"];
                    options.Listen(IPAddress.Any, defaultPort, listenOptions => {
                        // Port should only be 5000 on Kubernetes Cluster
                        if (httpsPort != defaultPort) {
                            //listenOptions.UseHttps(path, pass);
                        }
                    });
                })
                //.UseUrls($"https://0.0.0.0:{httpsPort}")
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseSerilog((_, config) => {
                    config
                    .MinimumLevel.Verbose()
                    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                    .Enrich.FromLogContext()
                    .WriteTo.File(@"Logs\log.txt", rollingInterval: RollingInterval.Day);
                })
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build()
                .Run();

        }

    }
}
