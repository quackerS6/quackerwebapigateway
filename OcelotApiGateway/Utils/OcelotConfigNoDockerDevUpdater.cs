﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;
using System;

namespace OcelotApiGateway.Utils {
    public static class OcelotConfigNoDockerDevUpdater {

        public static JObject UpdateOcelotConfigIfNoDockerDev(JObject ocelotJson, WebHostBuilderContext context) {
            if (context.HostingEnvironment.IsInDocker()) {
                return ocelotJson;
            }
            JArray routes = ocelotJson["Routes"] as JArray;
            foreach (JObject route in routes) {
                JArray downstreamHostsAndPorts = route["DownstreamHostAndPorts"] as JArray;
                foreach (JObject hostAndPort in downstreamHostsAndPorts) {
                    hostAndPort["Host"] = "localhost";
                }
            }
            return ocelotJson;
        }

        public static bool IsInDocker(this IWebHostEnvironment env) {
            return bool.Parse(Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER") ?? bool.FalseString);
        }
    }
}
