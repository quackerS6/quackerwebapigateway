﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace OcelotApiGateway.Utils {
    public static class ConfigurationBuilderExtensions {

        public static IConfigurationBuilder CreateJsonFile(this IConfigurationBuilder configBuilder, WebHostBuilderContext context, string sourceFolder, string destinationFile) {
            string basePath = context.HostingEnvironment.ContentRootPath;
            sourceFolder = Path.Combine(basePath, sourceFolder);
            destinationFile = Path.Combine(basePath, destinationFile);
            JObject output;
            DirectoryInfo start = Directory.CreateDirectory(sourceFolder);
            output = CreateJsonObjectFromFolder(start);
            output = OcelotConfigNoDockerDevUpdater.UpdateOcelotConfigIfNoDockerDev(output, context);
            File.WriteAllText(destinationFile, JsonConvert.SerializeObject(output, Formatting.Indented));
            return configBuilder;
        }

        private static JArray CreateJsonArrayFromFolder(DirectoryInfo folder) {
            JArray toReturn = new JArray();
            foreach (FileInfo file in folder.GetFiles()) {
                toReturn.Add(GetJsonFromFile(file));
            }

            foreach (DirectoryInfo subFolder in folder.GetDirectories()) {
                toReturn.Add(CreateJTokenFromFolder(subFolder));
            }

            return toReturn;
        }

        private static JObject CreateJsonObjectFromFolder(DirectoryInfo folder) {
            JObject toReturn = new JObject();
            foreach (FileInfo file in folder.GetFiles()) {
                toReturn.Add(Path.GetFileNameWithoutExtension(file.FullName), GetJsonFromFile(file));
            }

            foreach (DirectoryInfo subFolder in folder.GetDirectories()) {
                toReturn.Add(GetFolderName(subFolder), CreateJTokenFromFolder(subFolder));
            }

            return toReturn;
        }

        private static JToken CreateJTokenFromFolder(DirectoryInfo folder) {
            JToken toReturn;
            if (folder.Name.EndsWith("[]")) {
                toReturn = CreateJsonArrayFromFolder(folder);
            }
            else {
                toReturn = CreateJsonObjectFromFolder(folder);
            }
            return toReturn;
        }

        private static JObject GetJsonFromFile(FileInfo fileInfo) {
            using (StreamReader openFile = File.OpenText(fileInfo.FullName))
            using (JsonTextReader reader = new JsonTextReader(openFile)) {
                JObject readJson = (JObject)JToken.ReadFrom(reader);
                return readJson;
            }
        }

        private static string GetFolderName(DirectoryInfo dir) {
            if (dir.Name.EndsWith("[]")) {
                return dir.Name.Substring(0, dir.Name.Length - 2);
            }
            else {
                return dir.Name;
            }
        }

    }
}
