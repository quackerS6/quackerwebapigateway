﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using OcelotApiGateway.Auth;
using OcelotApiGateway.Serilog;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace OcelotApiGateway {
    public class Startup {

        public IConfiguration Configuration { get; }
        private readonly IWebHostEnvironment _env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env) {
            Configuration = configuration;
            _env = env;
        }

        public void ConfigureServices(IServiceCollection services) {

            string authenticationProviderKey = Configuration.GetSection("Auth0")["AuthKey"];
            string domain = Configuration.GetSection("Auth0")["Domain"];

            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(authenticationProviderKey, options => {
                    options.Authority = $"https://{domain}/";
                    options.Audience = Configuration.GetSection("Auth0")["Audience"];
                    options.TokenValidationParameters = new TokenValidationParameters {
                        NameClaimType = ClaimTypes.NameIdentifier
                    };
                });

            services.AddAuthorization(options => {
                options.AddPolicy("read:messages", policy => policy.Requirements.Add(new HasScopeRequirement("read:messages", domain)));
            });

            // Clear any transformations to the claim names, else Claims[sub] does not work when sending downstream
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();
            services.AddOcelot();

            // CORS
            services.AddCors(options => {
                options.AddPolicy("_allowWebApp", builder => {
                    builder
                    .WithOrigins(Configuration.GetSection("CORS")["WebApp"])
                    .AllowAnyHeader()
                    .SetPreflightMaxAge(TimeSpan.FromHours(2))
                    .AllowAnyMethod();
                });
            });

            if (_env.IsDevelopment()) {
                // Add development services
            }
            else {
                // Add production services
            }
        }

        public void Configure(IApplicationBuilder app) {
            if (_env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseMiddleware<RequestResponseLoggingMiddleware>();

            // Cors must be called before UseAuthorization
            app.UseCors("_allowWebApp");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseOcelot().Wait();


        }



    }
}
